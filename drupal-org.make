; $Id$

; Drupal core version.
core = 6.19

; OPL modules.
projects[qrs_sheets][version] = 1.0
projects[qrs_sheets][subdir]  = contrib

projects[covert_fields][version] = 1.0-rc3
projects[covert_fields][subdir]  = contrib

projects[xor_encryption][version] = 1.0
projects[xor_encryption][subdir]  = contrib

; Other modules.
projects[adminrole][version] = 1.2
projects[adminrole][subdir]  = contrib

projects[auto_nodetitle][version] = 1.2
projects[auto_nodetitle][subdir]  = contrib

projects[autoassignrole][version] = 1.2
projects[autoassignrole][subdir]  = contrib

projects[cck][version] = 2.8
projects[cck][subdir]  = contrib

projects[computed_field][version] = 1.0-beta3
projects[computed_field][subdir]  = contrib

projects[content_profile][version] = 1.0
projects[content_profile][subdir]  = contrib

projects[date][version] = 2.4
projects[date][subdir]  = contrib

projects[features][version] = 1.0-rc1
projects[features][subdir]  = contrib

projects[install_profile_api][version] = 2.1
projects[install_profile_api][subdir]  = contrib

projects[link][version] = 2.9
projects[link][subdir]  = contrib

projects[mobile_codes][version] = 1.1
projects[mobile_codes][subdir]  = contrib

projects[nodereferrer][version] = 1.0-rc1
projects[nodereferrer][subdir]  = contrib

projects[pathauto][version] = 1.4
projects[pathauto][subdir]  = contrib

projects[services][version] = 0.15
projects[services][subdir]  = contrib

projects[token][version] = 1.13
projects[token][subdir]  = contrib

projects[views][version] = 2.11
projects[views][subdir]  = contrib

projects[views_bulk_operations][version] = 1.9
projects[views_bulk_operations][subdir]  = contrib

projects[views_datasource][version] = 1.0-beta2
projects[views_datasource][subdir]  = contrib
