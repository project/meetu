<?php

/**
 * Helper to implementation of hook_content_default_fields().
 */
function _meetu_feature_p2p_content_default_fields() {
  $fields = array();

  // Exported field: field_mission_p2p
  $fields[] = array(
    'field_name' => 'field_mission_p2p',
    'type_name' => 'achievement_p2p',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'mission_p2p' => 'mission_p2p',
      'exhibit' => 0,
      'forum' => 0,
      'image' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'profile' => 0,
      'qr_sticker_sheet' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => NULL,
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_mission_p2p][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Mission',
      'weight' => '-4',
      'description' => 'The Player-to-Player mission that was completed.',
      'type' => 'nodereference_select',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_other_players
  $fields[] = array(
    'field_name' => 'field_other_players',
    'type_name' => 'achievement_p2p',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '5',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'qr_sticker_sheet' => 'qr_sticker_sheet',
      'exhibit' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'mission_p2p' => 0,
      'profile' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'validation_failure_message' => '',
      'validation_code' => '',
      'storage_code' => '$sharedKey  = meetuset_get_shared_code_key();

return xor_encryption_decrypt($entered_value, 36, NULL, $sharedKey);',
      'display_code' => '/*
 * If we encrypt what was stored, the result will be a different code than what
 * the user entered. So, to prevent confusion, we have to actually load the
 * sticker sheet that was given to the player, extract the private identifier
 * (i.e. the player\'s code) from it, and display *that value* instead.
 */
$stickerSheet = node_load($stored_value);

if (!empty($stickerSheet)) {
  $enteredValue = token_replace(\'[field_code_shared-raw]\', \'node\', $stickerSheet);

  return $enteredValue;
}',
      'autocomplete_match' => 'contains',
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Other Players (Sticker Sheets)',
      'weight' => '-3',
      'description' => 'The sticker sheet identifiers for the other players that the player connected with as part of completing the mission.',
      'type' => 'covert_field_widget',
      'module' => 'covert_fields',
    ),
  );

  // Exported field: field_player
  $fields[] = array(
    'field_name' => 'field_player',
    'type_name' => 'achievement_p2p',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'qr_sticker_sheet' => 'qr_sticker_sheet',
      'exhibit' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'mission_p2p' => 0,
      'profile' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'validation_failure_message' => '',
      'validation_code' => '',
      'storage_code' => '$sharedKey  = meetuset_get_shared_code_key();

return xor_encryption_decrypt($entered_value, 36, NULL, $sharedKey);',
      'display_code' => '/*
 * If we encrypt what was stored, the result will be a different code than what
 * the user entered. So, to prevent confusion, we have to actually load the
 * sticker sheet that was given to the player, extract the private identifier
 * (i.e. the player\'s code) from it, and display *that value* instead.
 */
$stickerSheet = node_load($stored_value);

if (!empty($stickerSheet)) {
  $enteredValue = token_replace(\'[field_code_shared-raw]\', \'node\', $stickerSheet);

  return $enteredValue;
}',
      'autocomplete_match' => 'contains',
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Player (Sticker Sheet)',
      'weight' => '-3',
      'description' => 'The sticker sheet for the player who completed this achievement.',
      'type' => 'covert_field_widget',
      'module' => 'covert_fields',
    ),
  );

  // Exported field: field_numstickers
  $fields[] = array(
    'field_name' => 'field_numstickers',
    'type_name' => 'mission_p2p',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '3',
    'max' => '5',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_numstickers][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Number of Stickers Required',
      'weight' => '2',
      'description' => 'The number of stickers the player will need from others in order to satisfy this mission.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_points
  $fields[] = array(
    'field_name' => 'field_points',
    'type_name' => 'mission_p2p',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '1',
    'max' => '100',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_points][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Points',
      'weight' => '1',
      'description' => 'The number of points the player receives for completing this mission.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_qr_image
  $fields[] = array(
    'field_name' => 'field_qr_image',
    'type_name' => 'mission_p2p',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'computed',
    'required' => '1',
    'multiple' => '0',
    'module' => 'computed_field',
    'active' => '1',
    'code' => '$numStickers = token_replace(\'[field_numstickers-raw]\', \'node\', $node);

switch ($numStickers)
{
	case 3:
		$layoutCode	= \'A\';
		break;
		
	case 4:
		$layoutCode	= \'B\';
		break;
		
	case 5:
		$layoutCode	= \'C\';
		break;
		
	default:
		die(\'Unexpected number of stickers: \' . $numStickers);
		break;
}

$nodeType	= token_replace(\'[type]\', \'node\', $node);

switch (strtolower($nodeType))
{
	case \'mission_p2e\':
		$typeCode	= \'E\';
		break;
		
	case \'mission_p2p\':
		$typeCode	= \'P\';
		break;
		
	default:
		die(\'Unexpected node type: \' . $nodeType);
		break;
}

$node_field[0][\'value\'] = token_replace("$layoutCode-$typeCode-[nid]", \'node\', $node);',
    'display_format' => '$display = theme(\'mobilecode\', $node_field_item[\'value\']);',
    'store' => 1,
    'data_type' => 'varchar',
    'data_length' => '15',
    'data_not_NULL' => 0,
    'data_default' => '',
    'data_sortable' => 1,
    'widget' => array(
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'QR Code',
      'weight' => '-4',
      'description' => 'The QR code for this mission.',
      'type' => 'computed',
      'module' => 'computed_field',
    ),
  );

  // Translatables
  array(
    t('Mission'),
    t('Number of Stickers Required'),
    t('Other Players (Sticker Sheets)'),
    t('Player (Sticker Sheet)'),
    t('Points'),
    t('QR Code'),
  );

  return $fields;
}

/**
 * Helper to implementation of hook_user_default_permissions().
 */
function _meetu_feature_p2p_user_default_permissions() {
  $permissions = array();

  // Exported permission: create achievement_p2p content
  $permissions[] = array(
    'name' => 'create achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: create mission_p2p content
  $permissions[] = array(
    'name' => 'create mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: delete any achievement_p2p content
  $permissions[] = array(
    'name' => 'delete any achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete any mission_p2p content
  $permissions[] = array(
    'name' => 'delete any mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: delete own achievement_p2p content
  $permissions[] = array(
    'name' => 'delete own achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete own mission_p2p content
  $permissions[] = array(
    'name' => 'delete own mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit any achievement_p2p content
  $permissions[] = array(
    'name' => 'edit any achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit any mission_p2p content
  $permissions[] = array(
    'name' => 'edit any mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_mission_p2p
  $permissions[] = array(
    'name' => 'edit field_mission_p2p',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit field_other_players
  $permissions[] = array(
    'name' => 'edit field_other_players',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit own achievement_p2p content
  $permissions[] = array(
    'name' => 'edit own achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit own mission_p2p content
  $permissions[] = array(
    'name' => 'edit own mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: view field_mission_p2p
  $permissions[] = array(
    'name' => 'view field_mission_p2p',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'mission manager',
      '4' => 'player',
    ),
  );

  // Exported permission: view field_other_players
  $permissions[] = array(
    'name' => 'view field_other_players',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  return $permissions;
}
