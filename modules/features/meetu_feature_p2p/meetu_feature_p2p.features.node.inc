<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _meetu_feature_p2p_node_info() {
  $items = array(
    'achievement_p2p' => array(
      'name' => t('Player-to-Player Achievement'),
      'module' => 'features',
      'description' => t('A Person-to-Person mission that a player has completed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'mission_p2p' => array(
      'name' => t('Player-to-Player Mission'),
      'module' => 'features',
      'description' => t('A mission for the player that involves connecting with other players.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Mission Instructions'),
      'min_word_count' => '1',
      'help' => t('Person-to-person missions should encourage the player to meet other players and get to know them. The mission instructions must be clear but concise, and inform the player exactly what he or she must do to complete the mission.'),
    ),
  );
  return $items;
}
