<?php

/**
 * Helper to implementation of hook_views_default_views().
 */
function _meetu_feature_p2p_views_default_views() {
  $views = array();

  // Exported view: _player_p2p_score
  $view = new view;
  $view->name = '_player_p2p_score';
  $view->description = 'This view is used internally by the rankings view to calculate a player\'s player-to-player mission score. ';
  $view->tag = 'meetü';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_player_nid' => array(
      'label' => 'Player (Sticker Sheet)',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_player_nid',
      'table' => 'node_data_field_player',
      'field' => 'field_player_nid',
      'relationship' => 'none',
    ),
    'nodereferer_referers' => array(
      'label' => 'Player Profile',
      'required' => 1,
      'referrer_delta' => '-1',
      'referrer_field' => 'field_sticker_sheets',
      'id' => 'nodereferer_referers',
      'table' => 'node',
      'field' => 'nodereferer_referers',
      'relationship' => 'field_player_nid',
    ),
    'field_mission_p2p_nid' => array(
      'label' => 'Mission',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_mission_p2p_nid',
      'table' => 'node_data_field_mission_p2p',
      'field' => 'field_mission_p2p_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'title' => array(
      'label' => 'Mission',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_mission_p2p_nid',
    ),
    'field_points_value' => array(
      'label' => 'Points',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_points_value',
      'table' => 'node_data_field_points',
      'field' => 'field_points_value',
      'relationship' => 'field_mission_p2p_nid',
    ),
  ));
  $handler->override_option('arguments', array(
    'uid' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => '',
      'wildcard_substitution' => '',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '6' => 0,
        '7' => 0,
      ),
      'relationship' => 'nodereferer_referers',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'qr_sticker_sheet' => 0,
        'achievement_p2e' => 0,
        'achievement_p2p' => 0,
        'exhibit' => 0,
        'mission_p2e' => 0,
        'mission_p2p' => 0,
        'page' => 0,
        'profile' => 0,
        'story' => 0,
        'test_page' => 0,
        'test_sheet' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'status_extra' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
      'relationship' => 'none',
    ),
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'achievement_p2p' => 'achievement_p2p',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'time',
    'results_lifespan' => '300',
    'output_lifespan' => '-1',
  ));
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'field_points_value' => 'field_points_value',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_points_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));

  $views[$view->name] = $view;

  // Exported view: completed_p2p_achievements
  $view = new view;
  $view->name = 'completed_p2p_achievements';
  $view->description = 'Shows the player the list of all player-to-player missions he or she, or another player, has completed.';
  $view->tag = 'meetü';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_sticker_sheets_nid' => array(
      'label' => 'Player\'s Sticker Sheets',
      'required' => 1,
      'delta' => '-1',
      'id' => 'field_sticker_sheets_nid',
      'table' => 'node_data_field_sticker_sheets',
      'field' => 'field_sticker_sheets_nid',
      'relationship' => 'none',
    ),
    'nodereferer_referers' => array(
      'label' => 'Achievement',
      'required' => 1,
      'referrer_delta' => '-1',
      'referrer_field' => 'field_player',
      'id' => 'nodereferer_referers',
      'table' => 'node',
      'field' => 'nodereferer_referers',
      'relationship' => 'field_sticker_sheets_nid',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'field_mission_p2p_nid' => array(
      'label' => 'Mission',
      'required' => 1,
      'delta' => -1,
      'id' => 'field_mission_p2p_nid',
      'table' => 'node_data_field_mission_p2p',
      'field' => 'field_mission_p2p_nid',
      'relationship' => 'nodereferer_referers',
    ),
  ));
  $handler->override_option('fields', array(
    'title' => array(
      'label' => 'Mission',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'field_mission_p2p_nid',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'body' => array(
      'label' => 'Description',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'body',
      'table' => 'node_revisions',
      'field' => 'body',
      'relationship' => 'field_mission_p2p_nid',
    ),
    'field_points_value' => array(
      'label' => 'Points',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_points_value',
      'table' => 'node_data_field_points',
      'field' => 'field_points_value',
      'relationship' => 'field_mission_p2p_nid',
    ),
    'phpcode' => array(
      'label' => 'Point Balance',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'strip_tags' => 0,
        'html' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'value' => '<?php
  global $_totalPoints;

  // Yes... the field really has that long a name.
  $_totalPoints += $data->node_node_data_field_mission_p2p_node_data_field_points_field_points_value;

  echo $_totalPoints;
?>',
      'sortable' => '0',
      'exclude' => 0,
      'id' => 'phpcode',
      'table' => 'customfield',
      'field' => 'phpcode',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'uid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(
        'count' => 1,
        'override' => 0,
        'items_per_page' => '25',
      ),
      'wildcard' => '',
      'wildcard_substitution' => '',
      'title' => 'Player-to-Player Missions Completed by %1',
      'breadcrumb' => '',
      'default_argument_type' => 'current_user',
      'default_argument' => '',
      'validate_type' => 'php',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'uid',
      'table' => 'users',
      'field' => 'uid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '7' => 7,
        '2' => 0,
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '8' => 0,
        '6' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '-1',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'profile' => 0,
        'achievement_p2e' => 0,
        'exhibit' => 0,
        'mission_p2e' => 0,
        'achievement_p2p' => 0,
        'mission_p2p' => 0,
        'qr_sticker_sheet' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 1,
      'validate_argument_php' => '$user = user_load($argument);

if (!empty($user)) {
  /* Only show achievements for players (users in the "player" role).
   *
   * We can\'t use the "User" validator with a role ID for this because the role ID could
   * differ between installations.
   */
  $valid = in_array(\'player\', $user->roles);
}
else {
  $valid = FALSE;
}

return $valid;',
      'override' => array(
        'button' => 'Override',
      ),
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'profile' => 'profile',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
    'status_extra' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
      'relationship' => 'nodereferer_referers',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('header', '<?php
  global $user;

  $view = views_get_current_view();
  $args = $view->args;

  // Display the following message if we\'re viewing the current user\'s achievements
  if (!isset($args[0]) || ($args[0] == $user->uid)) {
    ?>
This page lists all of the missions you completed during the game that involved interacting with other players, along with the number of points each mission was worth.
    <?php
  } else {
    ?>
This page lists all of the missions this player completed during the game that involved interacting with other players, along with the number of points that each mission was worth.
    <?php
  }
?>');
  $handler->override_option('header_format', '3');
  $handler->override_option('header_empty', 1);
  $handler->override_option('footer', '<?php
global $_totalPoints;
?>
<strong>Total Points from Player-to-Player Missions:</strong> <?php echo $_totalPoints; ?>');
  $handler->override_option('footer_format', '3');
  $handler->override_option('footer_empty', 0);
  $handler->override_option('empty', '<?php
  global $user;

  $view = views_get_current_view();
  $args = $view->args;

  // Display the following message if we\'re viewing the current user\'s achievements
  if (!isset($args[0]) || ($args[0] == $user->uid)) {
    ?>
Bummer! You didn\'t complete any player-to-player missions. The next time meetü is run, try to get out and meet some people.
    <?php
  } else {
    ?>
Bummer! This player isn\'t very social &ndash; he or she didn\'t complete any player-to-player missions.
    <?php
  }
?>');
  $handler->override_option('empty_format', '3');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'desc',
    'columns' => array(
      'title' => 'title',
      'body' => 'body',
      'field_points_value' => 'field_points_value',
      'phpcode' => 'phpcode',
    ),
    'info' => array(
      'title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'body' => array(
        'separator' => '',
      ),
      'field_points_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'phpcode' => array(
        'separator' => '',
      ),
    ),
    'default' => 'field_points_value',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'user/%/achievements/p2p');
  $handler->override_option('menu', array(
    'type' => 'default tab',
    'title' => 'Player-to-Player',
    'description' => 'View the list of completed missions that involved interacting with other players.',
    'weight' => '-1',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'tab',
    'title' => 'Achievements',
    'description' => 'View which missions this player completed during the game.',
    'weight' => '5',
  ));

  $views[$view->name] = $view;

  return $views;
}
