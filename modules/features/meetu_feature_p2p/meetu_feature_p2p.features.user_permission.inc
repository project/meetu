<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function meetu_feature_p2p_user_default_permissions() {
  $permissions = array();

  // Exported permission: create achievement_p2p content
  $permissions['create achievement_p2p content'] = array(
    'name' => 'create achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: create mission_p2p content
  $permissions['create mission_p2p content'] = array(
    'name' => 'create mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: delete any achievement_p2p content
  $permissions['delete any achievement_p2p content'] = array(
    'name' => 'delete any achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete any mission_p2p content
  $permissions['delete any mission_p2p content'] = array(
    'name' => 'delete any mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: delete own achievement_p2p content
  $permissions['delete own achievement_p2p content'] = array(
    'name' => 'delete own achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete own mission_p2p content
  $permissions['delete own mission_p2p content'] = array(
    'name' => 'delete own mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit any achievement_p2p content
  $permissions['edit any achievement_p2p content'] = array(
    'name' => 'edit any achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit any mission_p2p content
  $permissions['edit any mission_p2p content'] = array(
    'name' => 'edit any mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_mission_p2p
  $permissions['edit field_mission_p2p'] = array(
    'name' => 'edit field_mission_p2p',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit field_other_players
  $permissions['edit field_other_players'] = array(
    'name' => 'edit field_other_players',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit own achievement_p2p content
  $permissions['edit own achievement_p2p content'] = array(
    'name' => 'edit own achievement_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit own mission_p2p content
  $permissions['edit own mission_p2p content'] = array(
    'name' => 'edit own mission_p2p content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: view field_mission_p2p
  $permissions['view field_mission_p2p'] = array(
    'name' => 'view field_mission_p2p',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'mission manager',
      '4' => 'player',
    ),
  );

  // Exported permission: view field_other_players
  $permissions['view field_other_players'] = array(
    'name' => 'view field_other_players',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  return $permissions;
}
