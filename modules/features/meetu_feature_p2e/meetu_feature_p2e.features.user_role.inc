<?php

/**
 * Implementation of hook_user_default_roles().
 */
function meetu_feature_p2e_user_default_roles() {
  $roles = array();

  // Exported role: exhibit manager
  $roles['exhibit manager'] = array(
    'name' => 'exhibit manager',
  );

  return $roles;
}
