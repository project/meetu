<?php

/**
 * Implementation of hook_node_info().
 */
function meetu_feature_p2e_node_info() {
  $items = array(
    'achievement_p2e' => array(
      'name' => t('Player-to-Exhibit Achievement'),
      'module' => 'features',
      'description' => t('A Player-to-Exhibit mission that a player has completed.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
    'exhibit' => array(
      'name' => t('Exhibit'),
      'module' => 'features',
      'description' => t('An exhibit that players can visit as part of completing certain missions.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '1',
      'body_label' => t('Description'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'mission_p2e' => array(
      'name' => t('Player-to-Exhibit Mission'),
      'module' => 'features',
      'description' => t('A mission for the player that involves connecting with exhibits.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Mission Instructions'),
      'min_word_count' => '1',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function meetu_feature_p2e_views_api() {
  return array(
    'api' => '2',
  );
}
