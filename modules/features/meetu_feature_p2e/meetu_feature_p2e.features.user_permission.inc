<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function meetu_feature_p2e_user_default_permissions() {
  $permissions = array();

  // Exported permission: create achievement_p2e content
  $permissions['create achievement_p2e content'] = array(
    'name' => 'create achievement_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: create exhibit content
  $permissions['create exhibit content'] = array(
    'name' => 'create exhibit content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
    ),
  );

  // Exported permission: create mission_p2e content
  $permissions['create mission_p2e content'] = array(
    'name' => 'create mission_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: delete any achievement_p2e content
  $permissions['delete any achievement_p2e content'] = array(
    'name' => 'delete any achievement_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete any exhibit content
  $permissions['delete any exhibit content'] = array(
    'name' => 'delete any exhibit content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
    ),
  );

  // Exported permission: delete any mission_p2e content
  $permissions['delete any mission_p2e content'] = array(
    'name' => 'delete any mission_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: delete own achievement_p2e content
  $permissions['delete own achievement_p2e content'] = array(
    'name' => 'delete own achievement_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete own exhibit content
  $permissions['delete own exhibit content'] = array(
    'name' => 'delete own exhibit content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
    ),
  );

  // Exported permission: delete own mission_p2e content
  $permissions['delete own mission_p2e content'] = array(
    'name' => 'delete own mission_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit any achievement_p2e content
  $permissions['edit any achievement_p2e content'] = array(
    'name' => 'edit any achievement_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit any exhibit content
  $permissions['edit any exhibit content'] = array(
    'name' => 'edit any exhibit content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit any mission_p2e content
  $permissions['edit any mission_p2e content'] = array(
    'name' => 'edit any mission_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_eligible_exhibits
  $permissions['edit field_eligible_exhibits'] = array(
    'name' => 'edit field_eligible_exhibits',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_exhibit_contact_email
  $permissions['edit field_exhibit_contact_email'] = array(
    'name' => 'edit field_exhibit_contact_email',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
    ),
  );

  // Exported permission: edit field_exhibit_sticker_sheet
  $permissions['edit field_exhibit_sticker_sheet'] = array(
    'name' => 'edit field_exhibit_sticker_sheet',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit field_exhibits
  $permissions['edit field_exhibits'] = array(
    'name' => 'edit field_exhibits',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit field_location
  $permissions['edit field_location'] = array(
    'name' => 'edit field_location',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
    ),
  );

  // Exported permission: edit field_mission_p2e
  $permissions['edit field_mission_p2e'] = array(
    'name' => 'edit field_mission_p2e',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit own achievement_p2e content
  $permissions['edit own achievement_p2e content'] = array(
    'name' => 'edit own achievement_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit own exhibit content
  $permissions['edit own exhibit content'] = array(
    'name' => 'edit own exhibit content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
    ),
  );

  // Exported permission: edit own mission_p2e content
  $permissions['edit own mission_p2e content'] = array(
    'name' => 'edit own mission_p2e content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: view field_eligible_exhibits
  $permissions['view field_eligible_exhibits'] = array(
    'name' => 'view field_eligible_exhibits',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
      '2' => 'game manager',
      '3' => 'mission manager',
    ),
  );

  // Exported permission: view field_exhibit_contact_email
  $permissions['view field_exhibit_contact_email'] = array(
    'name' => 'view field_exhibit_contact_email',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
      '2' => 'game manager',
      '3' => 'mission manager',
    ),
  );

  // Exported permission: view field_exhibit_sticker_sheet
  $permissions['view field_exhibit_sticker_sheet'] = array(
    'name' => 'view field_exhibit_sticker_sheet',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'exhibit manager',
      '2' => 'game manager',
      '3' => 'mission manager',
    ),
  );

  // Exported permission: view field_exhibits
  $permissions['view field_exhibits'] = array(
    'name' => 'view field_exhibits',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: view field_location
  $permissions['view field_location'] = array(
    'name' => 'view field_location',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'exhibit manager',
      '3' => 'game manager',
      '4' => 'mission manager',
      '5' => 'player',
    ),
  );

  // Exported permission: view field_mission_p2e
  $permissions['view field_mission_p2e'] = array(
    'name' => 'view field_mission_p2e',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'mission manager',
      '4' => 'player',
    ),
  );

  return $permissions;
}
