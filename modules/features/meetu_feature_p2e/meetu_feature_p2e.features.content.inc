<?php

/**
 * Implementation of hook_content_default_fields().
 */
function meetu_feature_p2e_content_default_fields() {
  $fields = array();

  // Exported field: field_exhibits
  $fields['achievement_p2e-field_exhibits'] = array(
    'field_name' => 'field_exhibits',
    'type_name' => 'achievement_p2e',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '5',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'qr_sticker_sheet' => 'qr_sticker_sheet',
      'exhibit' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'mission_p2p' => 0,
      'profile' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'validation_failure_message' => 'The sticker sheet code "%entered_value" isn\'t recognized; please check and correct your entry.',
      'validation_code' => '',
      'storage_code' => '$sharedKey  = meetuset_get_shared_code_key();

return xor_encryption_decrypt($entered_value, 36, NULL, $sharedKey);',
      'display_code' => '/*
 * If we encrypt what was stored, the result will be a different code than what
 * the user entered. So, to prevent confusion, we have to actually load the
 * sticker sheet that was given to the player, extract the private identifier
 * (i.e. the player\'s code) from it, and display *that value* instead.
 */
$stickerSheet = node_load($stored_value);

if (!empty($stickerSheet)) {
  $enteredValue = token_replace(\'[field_code_shared-raw]\', \'node\', $stickerSheet);

  return $enteredValue;
}',
      'autocomplete_match' => 'contains',
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Exhibits (Sticker Sheets)',
      'weight' => '-3',
      'description' => 'The sticker code for each exhibit the player visited as part of completing the mission.',
      'type' => 'covert_field_widget',
      'module' => 'covert_fields',
    ),
  );

  // Exported field: field_mission_p2e
  $fields['achievement_p2e-field_mission_p2e'] = array(
    'field_name' => 'field_mission_p2e',
    'type_name' => 'achievement_p2e',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'mission_p2e' => 'mission_p2e',
      'exhibit' => 0,
      'forum' => 0,
      'image' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'achievement_p2p' => 0,
      'profile' => 0,
      'qr_sticker_sheet' => 0,
      'story' => 0,
      'mission_p2p' => FALSE,
      'test_page' => FALSE,
      'test_sheet' => FALSE,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => NULL,
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_mission_p2e][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Mission',
      'weight' => '-4',
      'description' => 'The Player-to-Exhibit mission that was completed.',
      'type' => 'nodereference_select',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_player
  $fields['achievement_p2e-field_player'] = array(
    'field_name' => 'field_player',
    'type_name' => 'achievement_p2e',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'qr_sticker_sheet' => 'qr_sticker_sheet',
      'exhibit' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'mission_p2p' => 0,
      'profile' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'validation_failure_message' => 'The sticker sheet code "%entered_value" isn\'t recognized; please check and correct your entry.',
      'validation_code' => '',
      'storage_code' => '$sharedKey  = meetuset_get_shared_code_key();

return xor_encryption_decrypt($entered_value, 36, NULL, $sharedKey);',
      'display_code' => '/*
 * If we encrypt what was stored, the result will be a different code than what
 * the user entered. So, to prevent confusion, we have to actually load the
 * sticker sheet that was given to the player, extract the private identifier
 * (i.e. the player\'s code) from it, and display *that value* instead.
 */
$stickerSheet = node_load($stored_value);

if (!empty($stickerSheet)) {
  $enteredValue = token_replace(\'[field_code_shared-raw]\', \'node\', $stickerSheet);

  return $enteredValue;
}',
      'autocomplete_match' => 'contains',
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Player (Sticker Sheet)',
      'weight' => '-3',
      'description' => 'The sticker sheet for the player who completed this achievement.',
      'type' => 'covert_field_widget',
      'module' => 'covert_fields',
    ),
  );

  // Exported field: field_exhibit_contact_email
  $fields['exhibit-field_exhibit_contact_email'] = array(
    'field_name' => 'field_exhibit_contact_email',
    'type_name' => 'exhibit',
    'display_settings' => array(
      'weight' => '-3',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_exhibit_contact_email][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Contact E-mail',
      'weight' => '-3',
      'description' => 'The e-mail address of the person to contact about this exhibit.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_exhibit_sticker_sheet
  $fields['exhibit-field_exhibit_sticker_sheet'] = array(
    'field_name' => 'field_exhibit_sticker_sheet',
    'type_name' => 'exhibit',
    'display_settings' => array(
      'weight' => '-2',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'qr_sticker_sheet' => 'qr_sticker_sheet',
      'exhibit' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'mission_p2p' => 0,
      'profile' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'validation_failure_message' => 'The sheet code "%entered_value" isn\'t recognized; please check and correct your entry.',
      'validation_code' => 'if (!function_exists(\'_meetu_validation_ensure_player_code_is_unique\')) {
  /**
   * Check to ensure that the specified player / sheet code is unique (i.e. the user hasn\'t entered the same code
   * twice).
   *
   * If the user hasn\'t, the appropriate form validation error will be raised.
   *
   * @param $playerCode
   *    The player / sheet code to check for uniqueness.
   *
   * @param $formElement
   *    The FAPI form element for the player code field.
   *
   * @param $errorElement
   *    The name of the element to raise form validation errors on.
   */
  function _meetu_validation_ensure_player_code_is_unique($playerCode, $delta, $formElement, $errorElement) {
    $fieldName          = $formElement[\'#field_name\'];
    $primaryColumnName  = $formElement[\'#columns\'][0];

    /* We need to ensure that each player code is unique, so we check all of the previous values for duplicates.
     *
     * We don\'t check the values that follow this value because that would result in two error messages per duplicate,
     * rather than just one (the first time a value appears shouldn\'t be considered an error).
     */
    for ($i = 0; $i < $delta; ++$i) {
      $currentValue = $formElement[\'#post\'][$fieldName][$i][$primaryColumnName];

      if ($currentValue == $playerCode) {
        form_set_error(
          $errorElement,
          t("You\'ve already entered sheet code %code. Please enter each code only once.",
            array(\'%code\' => $currentValue)));
      }
    }
  }
}

if (!function_exists(\'_meetu_validation_ensure_sheet_is_unused\')) {
  /**
   * Check to ensure that the specified player / sheet code is not already assigned to another player or exhibit.
   *
   * If the code is already in use, the appropriate form validation error will be raised.
   *
   * @param $playerCode
   *    The player / sheet code to check.
   *
   * @param $expectedExhibit
   *    The name of the exhibit that is expected to be associated with the specified player code.
   *
   * @param $errorElement
   *    The name of the element to raise form validation errors on.
   */
  function _meetu_validation_ensure_sheet_is_unused($playerCode, $expectedExhibit, $errorElement) {
    // Look-up all players that are using the specified player / sheet code
    $playerResults = _meetu_validation_execute_view_with_argument(\'_sticker_sheet_to_player\', $playerCode);

    if (!empty($playerResults)) {
      // Name of the player associated with the player code
      $playerName = $playerResults[0][\'name\'];

      form_set_error(
        $errorElement,
        t(\'The sheet code %code is already registered to the player %player.\',
          array(
            \'%code\'   => $playerCode,
            \'%player\' => $playerName)));
    }

    // Look-up all exhibits that are using the specified player / sheet code
    $exhibitResults = _meetu_validation_execute_view_with_argument(\'_sticker_sheet_to_exhibit\', $playerCode);

    if (!empty($exhibitResults)) {
      foreach ($exhibitResults as $row) {
        // Name of the exhibit associated with the player code
        $existingExhibitTitle = $row[\'title\'];

        if ($existingExhibitTitle != $expectedExhibit) {
          form_set_error(
            $errorElement,
            t(\'The sheet code %code is already registered to the exhibit %exhibit.\',
              array(
                \'%code\'     => $playerCode,
                \'%exhibit\'  => $existingExhibitTitle)));
          break;
        }
      }
    }
  }
}

if (!function_exists(\'_meetu_validation_execute_view_with_argument\')) {
  /**
   * Programmatically execute the specified view with the specified argument and returns the result as an array.
   *
   * @param $viewName
   *    The name of the view to execute.
   *
   * @param $argument
   *    The value to pass in for the first (and only) argument to the view.
   *
   * @return
   *    An array of the resulting rows from the view, each as an array indexed by field name; or, an empty array
   *    if there were no results or the view did not exist.
   */
  function _meetu_validation_execute_view_with_argument($viewName, $argument) {
    $view = views_get_view($viewName);

    if (!empty($view)) {
      $view->set_arguments(array($argument));
      $view->preview();

      $results  = $view->style_plugin->rendered_fields;
    }
    else {
      $results  = array();
    }

    return $results;
  }
}

// The name of this exhibit
$exhibitName = $element[\'#post\'][\'title\'];

// The name of the element to raise form validation errors on.
$errorElement = $element[\'_error_element\'][\'#value\'];

_meetu_validation_ensure_player_code_is_unique($entered_value, $delta, $element, $errorElement);
_meetu_validation_ensure_sheet_is_unused($entered_value, $exhibitName, $errorElement);',
      'storage_code' => '$playerKey  = meetuset_get_player_code_key();

return xor_encryption_decrypt($entered_value, 36, NULL, $playerKey);',
      'display_code' => '/*
 * If we encrypt what was stored, the result will be a different code than what
 * the user entered. So, to prevent confusion, we have to actually load the
 * sticker sheet that was given to the player, extract the private identifier
 * (i.e. the player\'s code) from it, and display *that value* instead.
 */
$stickerSheet = node_load($stored_value);

if (!empty($stickerSheet)) {
  $enteredValue = token_replace(\'[field_code_player-raw]\', \'node\', $stickerSheet);

  return $enteredValue;
}',
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Assigned Sheet Code(s)',
      'weight' => '-3',
      'description' => 'Enter the "sheet code" from each sticker sheet that has been assigned to this exhibit.',
      'type' => 'covert_field_widget',
      'module' => 'covert_fields',
    ),
  );

  // Exported field: field_location
  $fields['exhibit-field_location'] = array(
    'field_name' => 'field_location',
    'type_name' => 'exhibit',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_location][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Location',
      'weight' => '-4',
      'description' => 'The location of the exhibit.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_eligible_exhibits
  $fields['mission_p2e-field_eligible_exhibits'] = array(
    'field_name' => 'field_eligible_exhibits',
    'type_name' => 'mission_p2e',
    'display_settings' => array(
      'weight' => '1',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'exhibit' => 'exhibit',
      'forum' => 0,
      'image' => 0,
      'page' => 0,
      'achievement_p2e' => 0,
      'mission_p2e' => 0,
      'achievement_p2p' => 0,
      'profile' => 0,
      'qr_sticker_sheet' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => NULL,
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Eligible Exhibits',
      'weight' => '3',
      'description' => 'The booths that are eligible for completing this mission.',
      'type' => 'nodereference_select',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_numstickers
  $fields['mission_p2e-field_numstickers'] = array(
    'field_name' => 'field_numstickers',
    'type_name' => 'mission_p2e',
    'display_settings' => array(
      'weight' => 0,
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '3',
    'max' => '5',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_numstickers][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Number of Stickers Required',
      'weight' => '2',
      'description' => 'The number of stickers the player will need from others in order to satisfy this mission.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_points
  $fields['mission_p2e-field_points'] = array(
    'field_name' => 'field_points',
    'type_name' => 'mission_p2e',
    'display_settings' => array(
      'weight' => '-1',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '1',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '1',
    'max' => '100',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_points][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Points',
      'weight' => '1',
      'description' => 'The number of points the player receives for completing this mission.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_qr_image
  $fields['mission_p2e-field_qr_image'] = array(
    'field_name' => 'field_qr_image',
    'type_name' => 'mission_p2e',
    'display_settings' => array(
      'weight' => '-4',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'computed',
    'required' => '1',
    'multiple' => '0',
    'module' => 'computed_field',
    'active' => '1',
    'code' => '$numStickers = token_replace(\'[field_numstickers-raw]\', \'node\', $node);

switch ($numStickers)
{
	case 3:
		$layoutCode	= \'A\';
		break;
		
	case 4:
		$layoutCode	= \'B\';
		break;
		
	case 5:
		$layoutCode	= \'C\';
		break;
		
	default:
		die(\'Unexpected number of stickers: \' . $numStickers);
		break;
}

$nodeType	= token_replace(\'[type]\', \'node\', $node);

switch (strtolower($nodeType))
{
	case \'mission_p2e\':
		$typeCode	= \'E\';
		break;
		
	case \'mission_p2p\':
		$typeCode	= \'P\';
		break;
		
	default:
		die(\'Unexpected node type: \' . $nodeType);
		break;
}

$node_field[0][\'value\'] = token_replace("$layoutCode-$typeCode-[nid]", \'node\', $node);',
    'display_format' => '$display = theme(\'mobilecode\', $node_field_item[\'value\']);',
    'store' => 1,
    'data_type' => 'varchar',
    'data_length' => '15',
    'data_not_NULL' => 0,
    'data_default' => '',
    'data_sortable' => 1,
    'widget' => array(
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'QR Code',
      'weight' => '-4',
      'description' => 'The QR code for this mission.',
      'type' => 'computed',
      'module' => 'computed_field',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Assigned Sheet Code(s)');
  t('Contact E-mail');
  t('Eligible Exhibits');
  t('Exhibits (Sticker Sheets)');
  t('Location');
  t('Mission');
  t('Number of Stickers Required');
  t('Player (Sticker Sheet)');
  t('Points');
  t('QR Code');

  return $fields;
}
