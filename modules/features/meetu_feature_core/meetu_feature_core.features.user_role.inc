<?php

/**
 * Implementation of hook_user_default_roles().
 */
function meetu_feature_core_user_default_roles() {
  $roles = array();

  // Exported role: anonymous user
  $roles['anonymous user'] = array(
    'name' => 'anonymous user',
  );

  // Exported role: authenticated user
  $roles['authenticated user'] = array(
    'name' => 'authenticated user',
  );

  // Exported role: data provider
  $roles['data provider'] = array(
    'name' => 'data provider',
  );

  // Exported role: game manager
  $roles['game manager'] = array(
    'name' => 'game manager',
  );

  // Exported role: mission manager
  $roles['mission manager'] = array(
    'name' => 'mission manager',
  );

  // Exported role: player
  $roles['player'] = array(
    'name' => 'player',
  );

  return $roles;
}
