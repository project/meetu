<?php

/**
 * Implementation of hook_fieldgroup_default_groups().
 */
function meetu_feature_core_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_player_info
  $groups['profile-group_player_info'] = array(
    'group_type' => 'standard',
    'type_name' => 'profile',
    'group_name' => 'group_player_info',
    'label' => 'Player Information',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-4',
    'fields' => array(
      '0' => 'field_sticker_sheets',
    ),
  );

  // Exported group: group_social_networks
  $groups['profile-group_social_networks'] = array(
    'group_type' => 'standard',
    'type_name' => 'profile',
    'group_name' => 'group_social_networks',
    'label' => 'Social Networks',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-2',
    'fields' => array(
      '0' => 'field_twitter_username',
      '1' => 'field_facebook_url',
      '2' => 'field_myspace_url',
    ),
  );

  // Exported group: group_your_info
  $groups['profile-group_your_info'] = array(
    'group_type' => 'standard',
    'type_name' => 'profile',
    'group_name' => 'group_your_info',
    'label' => 'Getting to Know You',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-3',
    'fields' => array(
      '0' => 'field_birthdate',
      '1' => 'field_favorite_color',
      '2' => 'field_interests',
      '3' => 'field_favorite_music',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Getting to Know You');
  t('Player Information');
  t('Social Networks');

  return $groups;
}
