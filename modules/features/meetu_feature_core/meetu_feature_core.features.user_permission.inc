<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function meetu_feature_core_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration pages
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: access services
  $permissions['access services'] = array(
    'name' => 'access services',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'data provider',
    ),
  );

  // Exported permission: access user profiles
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: administer nodes
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: administer services
  $permissions['administer services'] = array(
    'name' => 'administer services',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: administer url aliases
  $permissions['administer url aliases'] = array(
    'name' => 'administer url aliases',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: administer users
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: change own username
  $permissions['change own username'] = array(
    'name' => 'change own username',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
    ),
  );

  // Exported permission: create QR-code PDF sticker sheet content
  $permissions['create QR-code PDF sticker sheet content'] = array(
    'name' => 'create QR-code PDF sticker sheet content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: create page content
  $permissions['create page content'] = array(
    'name' => 'create page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: create profile content
  $permissions['create profile content'] = array(
    'name' => 'create profile content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: create story content
  $permissions['create story content'] = array(
    'name' => 'create story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: create url aliases
  $permissions['create url aliases'] = array(
    'name' => 'create url aliases',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete any QR-code PDF sticker sheet content
  $permissions['delete any QR-code PDF sticker sheet content'] = array(
    'name' => 'delete any QR-code PDF sticker sheet content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete any page content
  $permissions['delete any page content'] = array(
    'name' => 'delete any page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete any profile content
  $permissions['delete any profile content'] = array(
    'name' => 'delete any profile content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete any story content
  $permissions['delete any story content'] = array(
    'name' => 'delete any story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete own QR-code PDF sticker sheet content
  $permissions['delete own QR-code PDF sticker sheet content'] = array(
    'name' => 'delete own QR-code PDF sticker sheet content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: delete own page content
  $permissions['delete own page content'] = array(
    'name' => 'delete own page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: delete own profile content
  $permissions['delete own profile content'] = array(
    'name' => 'delete own profile content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: delete own story content
  $permissions['delete own story content'] = array(
    'name' => 'delete own story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit any QR-code PDF sticker sheet content
  $permissions['edit any QR-code PDF sticker sheet content'] = array(
    'name' => 'edit any QR-code PDF sticker sheet content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit any page content
  $permissions['edit any page content'] = array(
    'name' => 'edit any page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit any profile content
  $permissions['edit any profile content'] = array(
    'name' => 'edit any profile content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit any story content
  $permissions['edit any story content'] = array(
    'name' => 'edit any story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit field_birthdate
  $permissions['edit field_birthdate'] = array(
    'name' => 'edit field_birthdate',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_code_player
  $permissions['edit field_code_player'] = array(
    'name' => 'edit field_code_player',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit field_code_shared
  $permissions['edit field_code_shared'] = array(
    'name' => 'edit field_code_shared',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit field_facebook_url
  $permissions['edit field_facebook_url'] = array(
    'name' => 'edit field_facebook_url',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_favorite_color
  $permissions['edit field_favorite_color'] = array(
    'name' => 'edit field_favorite_color',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_favorite_music
  $permissions['edit field_favorite_music'] = array(
    'name' => 'edit field_favorite_music',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_interests
  $permissions['edit field_interests'] = array(
    'name' => 'edit field_interests',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_myspace_url
  $permissions['edit field_myspace_url'] = array(
    'name' => 'edit field_myspace_url',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_numstickers
  $permissions['edit field_numstickers'] = array(
    'name' => 'edit field_numstickers',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_player
  $permissions['edit field_player'] = array(
    'name' => 'edit field_player',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'data provider',
      '2' => 'game manager',
    ),
  );

  // Exported permission: edit field_points
  $permissions['edit field_points'] = array(
    'name' => 'edit field_points',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_qr_image
  $permissions['edit field_qr_image'] = array(
    'name' => 'edit field_qr_image',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'mission manager',
    ),
  );

  // Exported permission: edit field_sticker_sheets
  $permissions['edit field_sticker_sheets'] = array(
    'name' => 'edit field_sticker_sheets',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit field_twitter_username
  $permissions['edit field_twitter_username'] = array(
    'name' => 'edit field_twitter_username',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'player',
    ),
  );

  // Exported permission: edit own QR-code PDF sticker sheet content
  $permissions['edit own QR-code PDF sticker sheet content'] = array(
    'name' => 'edit own QR-code PDF sticker sheet content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: edit own page content
  $permissions['edit own page content'] = array(
    'name' => 'edit own page content',
    'roles' => array(
      '0' => 'administrator',
    ),
  );

  // Exported permission: edit own profile content
  $permissions['edit own profile content'] = array(
    'name' => 'edit own profile content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: edit own story content
  $permissions['edit own story content'] = array(
    'name' => 'edit own story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: generate QR-code PDF sticker sheets in bulk (also requires permission to create)
  $permissions['generate QR-code PDF sticker sheets in bulk (also requires permission to create)'] = array(
    'name' => 'generate QR-code PDF sticker sheets in bulk (also requires permission to create)',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: view all QR-code PDF sticker sheet content (warning: resource intensive)
  $permissions['view all QR-code PDF sticker sheet content (warning: resource intensive)'] = array(
    'name' => 'view all QR-code PDF sticker sheet content (warning: resource intensive)',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: view field_birthdate
  $permissions['view field_birthdate'] = array(
    'name' => 'view field_birthdate',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: view field_code_player
  $permissions['view field_code_player'] = array(
    'name' => 'view field_code_player',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: view field_code_shared
  $permissions['view field_code_shared'] = array(
    'name' => 'view field_code_shared',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: view field_facebook_url
  $permissions['view field_facebook_url'] = array(
    'name' => 'view field_facebook_url',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: view field_favorite_color
  $permissions['view field_favorite_color'] = array(
    'name' => 'view field_favorite_color',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: view field_favorite_music
  $permissions['view field_favorite_music'] = array(
    'name' => 'view field_favorite_music',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: view field_interests
  $permissions['view field_interests'] = array(
    'name' => 'view field_interests',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: view field_myspace_url
  $permissions['view field_myspace_url'] = array(
    'name' => 'view field_myspace_url',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  // Exported permission: view field_numstickers
  $permissions['view field_numstickers'] = array(
    'name' => 'view field_numstickers',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'mission manager',
      '4' => 'player',
    ),
  );

  // Exported permission: view field_player
  $permissions['view field_player'] = array(
    'name' => 'view field_player',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'mission manager',
    ),
  );

  // Exported permission: view field_points
  $permissions['view field_points'] = array(
    'name' => 'view field_points',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'mission manager',
      '4' => 'player',
    ),
  );

  // Exported permission: view field_qr_image
  $permissions['view field_qr_image'] = array(
    'name' => 'view field_qr_image',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'anonymous user',
      '2' => 'game manager',
      '3' => 'mission manager',
      '4' => 'player',
    ),
  );

  // Exported permission: view field_sticker_sheets
  $permissions['view field_sticker_sheets'] = array(
    'name' => 'view field_sticker_sheets',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
    ),
  );

  // Exported permission: view field_twitter_username
  $permissions['view field_twitter_username'] = array(
    'name' => 'view field_twitter_username',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'game manager',
      '2' => 'player',
    ),
  );

  return $permissions;
}
