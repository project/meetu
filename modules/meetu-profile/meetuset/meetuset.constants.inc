<?php

/**
 * @file
 * Constants used in various files related to the meetuset module.
 *
 * @author
 * John Karahalis (john.karahalis@rit.edu)
 */

/**
 * Path to the meetuset settings page.
 */
define('MEETUSET_SETTINGS_PATH', 'admin/settings/meetu-settings');

/**
 * Name of the Drupal variable used to store the public key.
 */
define('MEETUSET_VAR_SHARED', 'meetuset_shared_code_key');

/**
 * Name of the Drupal variable used to store the private key.
 */
define('MEETUSET_VAR_PLAYER', 'meetuset_player_code_key');

/**
 * Name of the Drupal variable used to store the original (or "install-time") public key.
 */
define('MEETUSET_VAR_SHARED_ORIG', 'meetuset_shared_code_key_orig');

/**
 * Name of the Drupal variable used to store the original (or "install-time") private key.
 */
define('MEETUSET_VAR_PLAYER_ORIG', 'meetuset_player_code_key_orig');
