
The meetü Game Platform is an installer (or "Installation Profile") for meetü, a
Drupal-based social networking game that encourages people to connect with one
another.


Table of Contents
1. Downloading
2. Installation
3. Configuration
4. Known Issues
5. Credits


========================================
1. Downloading
========================================
There are three main package types of the meetü Game Platform available for
download.

For most purposes, the Core Package should be used to install a new meetü
website. This package is available for download directly from the "Downloads"
section of the meetü Game Platform project page on the Drupal website:
http://drupal.org/project/meetu

To download other packages, visit the meetü Game Platform project page and click
the "Notes" link for supported release (in green) of the platform. A list of
packages should be displayed on the page that loads.

Package types are outlined below.

Core Package (e.g., meetu-6.x-1.0-core.tar.gz)
----------------------------------------------
This is the package listed directly on the meetü project page in the "Downloads"
section in green.

This package includes the meetü installation scripts, a copy of all modules that
meetü depends upon, and a copy of Drupal core. For most purposes, this is the
package that should be used to install a new copy of the meetü Game Platform. If
you already have a Drupal website on your webserver or want greater control over
where meetü software is installed, use one of the packages described below.


No-Core Package (e.g., meetu-6.x-1.0-no-core.tar.gz)
----------------------------------------------------
This package includes only the meetü installation scripts and a copy of all
modules that meetü depends upon. Use this package if you already have a Drupal
installation on your webserver and would like meetü to be based off of that
instance of Drupal, but you do not have already have a copy of all modules that
meetü depends upon.

To use this package, extract it to the /profiles directory of your Drupal
installation and create a new website based on your existing Drupal installation
using a normal multisite setup process. For more information on Drupal
multisite setup, see http://drupal.org/node/346385


Minimal Package (e.g., meetu-6.x-1.0.tar.gz)
--------------------------------------------
This package includes only the meetü installation scripts and a copy of
custom modules used by meetü that are not available from drupal.org (e.g., the
"meetü Settings" module, Features modules that meetü uses, etc.).

Use this package if you would like to install meetü in a multisite environment
and already have a copy of all modules that meetü depends upon, or if you would
just like greater control over where the meetü modules are located.


========================================
2. Installation
========================================
Because the meetü Game Platform is based on Drupal, it can be installed almost
exactly like any other Drupal website. For this reason, it may be helpful to
refer to the standard Drupal installation guide:
http://drupal.org/node/628292

There some differences between the standard Drupal installation process and the
meetü installation process. These differences are outlined below.

* Whereas the standard Drupal installation guide instructs you to download and
  extract a standard Drupal package to your webserver, the meetü installation
  process requires you to download and extract a meetü core package (as
  described in Section 1) instead.

* Running the installation script
  (see http://drupal.org/getting-started/6/install/run-script)
  is slightly different when installing meetü. The installation "wizard" for a
  standard Drupal package will install a base Drupal platform by default. The
  installation wizard for meetü will ask you if you would like to install a
  meetü platform or a base Drupal platform. Ensure that "meetü" is selected at
  this screen if you would like to install the meetü Game Platform.

* When on the "Configure site" page of the installation wizard, the meetü Game
  Platform installer will provide some helpful defaults for things such as the
  site name and the username of the administrator account. These are only
  suggestions -- you are free to change these settings.

* After the installation is complete, you should receive a notice providing you
  with the automatically-generated username, password, and email address for a
  user account that can be used by Mission Card Processor software. It might be
  helpful to write this information down, but if you ever lose or want to change
  the account details, you can do so at any time by navigating to Administer >
  User management > Users on your meetü website.

Aside from those few differences, installing the meetü Game Platform should be
almost exactly like installing a standard Drupal website.

The installation process is similar if you are using a different meetü package,
such as a non-core package or a minimal package. For more information on the
specifics of these packages and how they affect the installation process, please
see Section 1.


========================================
3. Configuration
========================================
One of the major configuration pages for the meetü Game Platform exists at
Administer > Site building > Features. On this page, you can enable and disable
specific features of the meetü Game Platform. For example, if you do not want
your meetü website to allow Player-to-Exhibit missions, you can disable those
features on this page. 

For more information on configuring and managing your meetü website, please
refer to the meetü Game Platform documentation:
http://drupal.org/node/697388

Because meetü is based on Drupal, managing and configuring a meetü website is
similar in most ways to managing and configuring a standard Drupal website. For
more information on managing a Drupal website, please see the Drupal
Administration Guide:
http://drupal.org/node/627152


========================================
4. Known Issues
========================================
There are some known issues with the meetü Game Platform. Because these issues
are relatively minor and related to modules beyond the control of the meetü
developers, they were not able to be resolved in this release.

Known issues are outlined below, with a summary and workaround for each.

Different version of Views Custom Field module is recommended
-------------------------------------------------------------
Summary:    The meetü Game Platform ships with version 6.x-1.x-dev of the Views
            Custom Field module. After you have installed your site, you may
            receive a notice explaining that a different version of this module,
            version 6.x-1.0, is recommended.
            
Workaround: Do not install version 6.x-1.0 of Views Custom Field. The meetü
            Game Platform requires some features of Views Custom Field that are
            not available in version 6.x-1.0.

            Because this notice is a warning (yellow) and not an error (red), it
            can safely be ignored indefinitely. When version 6.x-1.1 of Views
            Custom Field is released, it will most likely contain all of the
            features that the meetü Game Platform requires. If it does, the
            meetü Game Platform will begin using that version and this issue
            will be resolved.


No available releases found for Features
----------------------------------------
Summary:    Features shipped with the meetü Game Platform (meetü Core,
            Player-to-Exhibit Missions, and Player-to-Player Missions) do not
            currently have any supported releases available for download from a
            Feature server due to a small bug on the Feature server that they
            use. This will cause the "Available updates" page of your meetü
            website to list these Features with a gray background and the text
            "No available releases found".

Workaround: None is necessary. By the time new versions of meetü Features are
            available, the issue with the Feature server should be resolved and
            your meetü website should properly report the status of your
            Features.


Bundled modules exist at uncommon location
------------------------------------------
Summary:    Contributed modules shipped with the meetü Game Platform exist in
            the directory profiles/meetu/modules/modules/contrib instead of the
            more standard directory profiles/meetu/modules/contrib. The
            duplicate "modules" directory seems to be related to a bug in Drush
            Make, the tool used by drupal.org to package installation profiles.

            A bug report has been submitted about this issue:
            http://drupal.org/node/675162#comment-2471490

Workaround: None is needed, as the location of the modules does not change their
            behavior. Just be aware of their location so that you can find them
            when they need to be updated.


========================================
5. Credits
========================================
meetü started as "The Social Networking Game", a proof-of-concept game for
connecting people based on similar interests. This list includes all people
who have contributed to the history of the meetü platform, including those who
worked on the original Social Networking Game.

Faculty Lead
* Professor Michael Riordan

Social Networking Game
* Professor Matthew Bernius (Faculty Lead)
* Abdulrahman Matsah
* Jacob Weigand
* Gordon Toth
* Michael Rubits

meetü Game Platform
* Guy Paddock
* John Karahalis
* Gordon Toth
* Justin Monsees
* Micah Stupak

meetü Brand Identity, Marketing, Promitional Media, and Game Materials
* Dan Casazza
* Mike Morisco
* Tony Stuck


meetü is a project of the Open Publishing Lab at the Rochester Institute of
Technology.

opl.rit.edu
opl.rit.edu/project/meetu
